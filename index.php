<!DOCTYPE html>
<html lang="en">

    <head>
	<meta charset="utf-8">
	<title>DistroKid simple analytics</title>

	<!--

	     A simple hack to get a better view of yourDistroKid stats.
	     See http://gitlab.com/novadeviator/distrokid-analytics/ for
	     the full code and how to use it by yourself on your server.

	-->
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

	<!-- masonry grid -->
	<script src="https://npmcdn.com/masonry-layout@4.0/dist/masonry.pkgd.min.js"></script>

	<style>

	 table {
	     border-spacing: 20px 2px;
	     border-collapse: separate;
	     width:100%;
	     padding-bottom:30px;
	 }

	 .grid-item { width:300px; }

	 h1 {
	     margin-bottom: 0;
	     display: inline;
	 }
	 
	</style>

	
    </head>
    
    <body>
	<div class="container-fluid">
	    
	    <h1>distrokid analytics</h1>
	    [ <small><A href="https://gitlab.com/novadeviator/distrokid-analytics/" target="_blank">code</A></small> ]

	    
	    <div class="grid" data-masonry='{ "itemSelector": ".grid-item", "columnWidth": 100 }'>

		
		<?php

		$dir = "tsv/"; // you can change this folder. it where tsv reports live

		// open directory, and read its contents
		if (is_dir($dir)) {
		    if ($dh = opendir($dir)) {
			while (($file = readdir($dh)) !== false) {
			    if ($file != ".") { 
				if ( $file != "..") {

				    // for each line in each tsv file create an array of items
				    if (($handle = fopen("tsv/$file", "r")) !== FALSE) {
					while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {
					    if ($data[0] != "Reporting Date") {
						
						$tds[] = $data;
						
					    }
					}
					fclose($handle);
				    }

				    $files[] = $file;

				}
			    }
			}
			closedir($dh);
		    }
		}
		

		$allearnings = 0;

		foreach ($tds as $key => $val) {
		    
		    $allearnings = $val[12] + $allearnings;
		    @$stores[$val[2]] = $stores[$val[2]] + $val[12];
		    @$artists[$val[3]] = $artists[$val[3]] + $val[12];
		    @$titles[$val[4]] = $titles[$val[4]] + $val[12];
		    @$countries[$val[10]] = $countries[$val[10]] + $val[12];
		    @$months[$val[1]] = $months[$val[1]] + $val[12];

		}

		// include misspellings group file, if it exists
		//$groups = "groups.php";
		//file_exists($groups) AND include $groups; 
		
		// sort all the lists ASCENDING
		arsort($stores);
		arsort($artists);
		arsort($titles);
		arsort($countries);
		arsort($months);


		// lets get htmlirty (html-dirty):
		$tableseparator  = "--------------------------";
		?>

		<div class="grid-item">
		    <table>

			<tr><td colspan=2><?php echo $tableseparator; ?></td></tr>
			<tr><th>TITLES</th><th></th></tr>
			<?php
			foreach ($titles as $key => $val){
			    $val = sprintf('%02.2f', $val);
			    echo "<tr><td>$key</td><td align=\"right\">$val</td></tr>\n\t\t\t";
			}
			?>

		    </table>
		</div>


		<div class="grid-item">
		    <table>

			<tr><td colspan=2><?php echo $tableseparator; ?></td></tr>
			<tr><th>COUNTRIES</th><th></th></tr>
			<?php 
			foreach ($countries as $key => $val){
			    $val = sprintf('%02.2f', $val);
			    echo "<tr><td>$key</td><td align=\"right\">$val</td></tr>\n\t\t\t";
			}
			echo "\n";
			?>
		    </table>
		</div>

		
		<div class="grid-item">
		    <table>
			
			<tr><td colspan=2><?php echo $tableseparator; ?></td></tr>
			<tr><th>STORES</th><th></th></tr>
			<?php 
			foreach ($stores as $key => $val){
			    $val = sprintf('%02.2f', $val);
			    echo "<tr><td>$key</td><td align=\"right\">$val</td></tr>\n\t\t\t";
			}
			?>
			
		    </table>
		</div>

		
		<div class="grid-item">
		    <table>
			
			<tr><td colspan=2><?php echo $tableseparator; ?></td></tr>
			<tr><th>ARTISTS</th><th></th></tr>
			<?php 
			foreach ($artists as $key => $val){
			    $val = sprintf('%02.2f', $val);
			    echo "<tr><td>$key</td><td align=\"right\">$val</td></tr>\n\t\t\t";
			}
			?>

		    </table>
		</div>


		<div class="grid-item">
		    <table>

			<tr><td colspan=2><?php echo $tableseparator; ?></td></tr>
			<tr><th>MONTHS</th><th></th></tr>
			<?php 
			foreach ($months as $key => $val){
			    $val = sprintf('%02.2f', $val);
			    echo "<tr><td>$key</td><td align=\"right\">$val</td></tr>\n\t\t\t";
			}
			?>
			
		    </table>
		</div>

		<!-- 
		<div class="grid-item">
		    <table>
			<tr><td colspan=2><?php echo $tableseparator; ?></td></tr>

			<tr><th>FILES</th><th></th></tr>
			<?php 
			foreach ($files as $key => $val){
			    //$val = sprintf('%02.2f', $val);
			    echo "<tr><td>$key</td><td align=\"right\">$val</td></tr>";
			} 
			?>
		    </table>
		</div>
		-->

		<div class="grid-item">
		    <table>
			<tr><td colspan=2><?php echo $tableseparator; ?></td></tr>
			<tr><td>ALL EARNINGS:  </td><td align="right"><strong><?php echo "$ ".sprintf('%02.2f', $allearnings); ?></strong></td></tr>
		    </table>
		</div>

	    </div>
	</div>
    </body>
</html>
