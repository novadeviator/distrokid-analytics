
# DISTROKID analytics


## ABOUT

A fast simple poor man's script hack (PHP/CSS/JS) interpreting the TSV (tab separated value) files from DistroKid to get slightly more sense in terms of
analytics.


## REQUIREMENTS

  * Webserver with PHP interpreter.
  * Browser.



## INSTALLATION


  * Put this script (index.php) in a folder accessible by webserver with PHP 
    interpreter.

  * Download TSV files from which you want data from DistroKid (you can get them at https://distrokid.com/bank/details/ (click small download icon next to [DISPLAY] button) and put them in a tsv/ subfolder. Make sure you don't duplicate any data with exports of different types of data (Sales Date / Reporting Date).
	
  * Check groups.php and set your grouped duplicates (it's hackish, I know!)
 
  * Access root web folder (where the index.php resides) with a browser.
  

## SCREENSHOT
  
![](https://gitlab.com/novadeviator/distrokid-analytics/raw/master/distrokidanalitics.png)


## AUTHOR

Copyright Luka Princic / Nova deViator <nova@deviator.si>.
Feel free to use and derive from it under conditions set forth by GNU GPL
licence. Improvements, patches, forks, pull requests welcome.

